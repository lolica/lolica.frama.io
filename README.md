# Site Internet de l'association LoLiCA

Ce dépôt Git a pour objectif de simplifier le déploiement du site internet de l'association LoLiCA.

Le site internet est accessible ici : [https://www.lolica.org/](https://www.lolica.org) 

## Qui sommes-nous ?

![](static/images/Logo.png)

Créée en 2003, LoLiCA est une association Loi 1901 ayant pour but la promotion des Logiciels Libres et des libertés numériques en Champagne-Ardenne.

Basée sur Reims, l'association est un rassemblement d'utilisateurs Linux et des Logiciels Libres.

## Installation et déploiement

Le site est entièrement codé avec le logiciel [Hugo](https://www.gohugo.io) et le thème [PaperMod](https://github.com/adityatelange/hugo-PaperMod).

Après avoir cloné le dépôt, il est nécssaire d'inclure le thème sous la forme d'un sous-module

```bash
git submodule update --init --recursive
```

Une pré-visualisation du site est possible en exécutant le serveur intégré à Hugo.

```bash
hugo server
```

Pour générer le site minifié dans le dossier `./public/` :
````
hugo --minify
````

## Objectif du dépôt

Ce dépôt utilise Gitlab Pages pour héberger les pages du site Internet.

Ceci, combiné à Hugo a pour objectif de :
- Automatiser et simplifier les mises à jour du site
- Suivre les changements apportés et les annuler si besoin
- Faciliter la relecture des nouveaux contenus avant leurs publication
- Permettre à plusieurs membres de l'association de pouvoir participer au développement du site
- Avoir une ou plusieurs copies locale du projet
- Simplifier la rédaction des articles en utilisant du Markdown
- Réduire la puissance requise pour héberger le site
