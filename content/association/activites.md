---
weight: 10
title: Nos activités
draft: false
---

Tout au long de l'année nous menons diverses actions sur Reims et sa région.

{{< figure 
     src = "/images/P9JDPP8TDXSK.png"
     link = "https://www.framapiaf.org/@gul_lolica"
     alt = "Photo de profil du compte Mastodon de LoLiCA"
     caption = "Restez informés de nos futures actions sur notre profil [Mastodon](https://www.framapiaf.org/@gul_lolica)."
     target = "_blank"
     align = "center"
     >}}

## Les ateliers

Centrés sur des questions techniques ou de protection de la vie privée, les ateliers permettent de s'ouvrir à divers sujets et aussi de les expérimenter.

[Alternative]: # (Les Ateliers sont des rencontres régulières qui offrent un espace d’échange et de sensibilisation autour des enjeux liés aux logiciels libres et à la protection de la vie privée)
[Alternative]: # (Ces ateliers ouverts à tous ont pour but de sensibiliser le grand public aux concepts fondamentaux des libertés numériques, tels que la confidentialité des données, la sécurité en ligne et l’importance des logiciels libres.)

## Les tables rondes

Nos tables-rondes sont l'occasion d'échanger autours de sujets très divers sur les thèmes des logiciels libres et des libertés numériques. L'objectif est de favoriser les échanges de connaissances et de permettre à chacun de mieux comprendre les différentes perspectives sur les thèmes abordés. 

Une petite sélection des sujets précédemment abordés :

- La place du numérique et des logiciels libres dans notre quotidien
- L'intelligence artificielle
- Le Bitcoin
- [...]

Elles se déroulent généralement en ligne.

## Les Glous

Glou glou glou !

Retrouvez-nous régulièrement autours d'un verre pour passer de bonnes soirées avec nous !

Des rendez-vous pour venir bavarder de tout et de rien (surtout de rien) et pour passer un bon moment.

[Alternative]: # (Nos Glous sont des moments où les membres de notre association se retrouvent dans un cadre informel, souvent dans un bar ou un café, pour partager des discussions et des moments de détente.)