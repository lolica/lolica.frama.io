---
weight: 30
title: Les statuts de l'association
draft: false
ShowToc: true
TocOpen: true 
---
> Révision au 14 mars 2020

## ARTICLE 1 - Constitution et dénomination
Il est fondé entre les adhérents aux présents statuts une association régie par la loi du 1er juillet 1901, ayant pour titre : LOLICA : Logiciel Libres en Champagne-Ardenne

## ARTICLE 2 - Buts
Cette association a pour but la promotion, dans le cadre de la région Champagne-Ardenne, des logiciels dont la licence permet, librement, l'utilisation, l'étude, la modification et la distribution du produit exécutable, de ses codes sources et de sa documentation.

## ARTICLE 3 - Siège social
Le siège social est fixé dans la ville de Reims.

Il pourra être transféré par simple décision du Conseil d'Administration et figurera in extenso dans le règlement intérieur.

## ARTICLE 4 - Durée de l'association
La durée de l'association est illimitée.

## ARTICLE 5 - Moyens d'action
Les moyens d'action de l'association sont notamment :
- Les publications,
- Les cours, les conférences,
- Les réunions de travail,
- L'organisation de manifestation, conformément à la législation en vigueur, tout autre initiative pouvant aider à la réalisation de l'objet de l'association.

## ARTICLE 6 - Ressources de l'association
Les ressources se composent :
- Des cotisations des membres,
- Des dons de tout type, conformément à la législation en vigueur,
- Des subventions qui pourront lui être accordées par les structures de l'Union Européenne, de l'État, des collectivités locales, des collectivités publiques ou des établissements publics, ainsi que l'association ou tout autre personne morale dans les conditions légales,
- De recette provenant de la vente de  produits, de services ou de prestations fournies par l'association, toute autre ressource qui ne soit pas contraire aux règles en vigueur.

## ARTICLE 7 - Composition de l'association
L'association se compose de :
- Membres actifs ou adhérents : Sont membres actifs ceux qui sont à jour de leur cotisation annuelle. Ils ont le droit à l'Assemblée Générale.
- Membres d'honneur : Sont membres d'honneur ceux qui ont rendu des services signalés à l'association. Ils sont dispensés de cotisations mais n'ont pas le droit de vote à l'Assemblée Générale.

## ARTICLE 8 - Admission et adhésion
Pour faire partie de l'association il faut adhérer aux présents statuts et s'acquitter de la cotisation dont le montant est fixé par l'Assemblée Générale.

Le conseil d'Administration pourra refuser des adhésions, avec avis motivé aux intéressés.

## ARTICLE 9 - Perte de la qualité de membre
La qualité de membre se perd par :
- La démission;
- Le décès;
- La radiation prononcée par le Conseil d'Administration pour non paiement de la cotisation ou pour motif grave.

## ARTICLE 10 - Assemblée Générale Ordinaire
L'assemblée Générale Ordinaire se réunit au moins une fois par an et comprend tout les membre de l'association à jour de leur cotisation.

Quinze jours au moins avant la date fixée, les membres de l'association sont convoqués à la demande du Président ou du Conseil d'Administration, ou du tiers des membres de l'association. L'ordre du jour est indiqué sur les convocations.

L'assemblée Générale, après avoir délibéré, se prononce sur le rapport moral ou d'activité et sur les comptes de l'exercice financier. Elle délibère sur les orientations à venir.

Elle pourvoit à la nomination ou au renouvellement des membres du conseil d'Administration.

Elle fixe aussi le montant de la cotisation annuelle.

Les décisions de l'Assemblée sont prises à la majorité des membres présents.

## ARTICLE 11 - Conseil d'Administration
L'association est dirigée par un Conseil d'administration composé de 7 membres au maximum, élus pour 2 années par l'Assemblée Générale.

Les membres sont rééligibles. Le Conseil d’Administration étant renouvelé chaque année par moitié, la première année, les membres sortant sont désignés par le sort.

En cas de vacance de poste, le conseil d’administration pourvoit provisoirement au remplacement de ses membres. Il est procédé à leur remplacement définitif à la plus prochaine Assemblée Générale. Les pouvoirs des membres ainsi élus prennent fin à l'époque où devrait normalement expirer le mandat des membres remplacés.

Les mineurs de plus de 16 ans sont éligibles au Conseil d’Administration mais non au bureau.

Le conseil d’Administration se réunit au moins 1 fois par an et toutes les fois qu'il est convoqué par le Président ou au moins un quart de ses membres.

Les décisions sont prises à la majorité des voix des présents. En cas de partage, la voix du Président est prépondérante. La présence d'au moins la moitié des membres est nécessaire pour que le Conseil d'Administration puisse délibérer valablement.

Le conseil d'administration choisit parmi ses membres, au scrutin secret, un Bureau composé de :
- un Président et, si besoin, un ou plusieurs Vice-Présidents;
- un Secrétaire et, si besoin, un Secrétaire Adjoint;
- un Trésorier et, si besoin, un Trésorier Adjoint.

## ARTICLE 11A - Rôle des membres du bureau
Le président convoque les assemblées générales et les réunions du conseil d'administration. Il représente l'association dans tous les actes de la vie civile et est investi des pouvoirs à cet effet. Il représente l'association en justice. En cas d'absence ou de maladie, il est remplacé par le vice-président qui dispose alors des mêmes pouvoirs.

Le secrétaire est chargé de tout ce qui concerne la correspondance et les archives. Il rédige les procès-verbaux des délibérations du conseil et en assure la transcription sur les registres. En cas d'absence ou de maladie, il est remplacé par un membre du conseil désigné par le président qui dispose alors des mêmes pouvoirs.

Le trésorier est chargé de tout ce qui concerne la gestion du patrimoine de l'association. Il effectue tous paiement et perçoit toutes recettes sous la surveillance du président. Il tient une comptabilité régulière de toutes les opérations. En cas d'absence ou de maladie, il est remplacé par un membre du conseil désigné par le président qui dispose alors des mêmes pouvoirs.

## ARTICLE 12 - Rémunération, représentation et prestation
Les membres du Conseil d'Administration ne peuvent recevoir aucune rétribution à raison des fonctions qui leur sont conférées.

Cependant, les frais et débours occasionnés pour l'accomplissement du mandat d'administrateur sont remboursés au vu des pièces justificatives. Le rapport financier présenté à l'assemblée Générale Ordinaire doit faire mention des remboursements de frais de mission, de déplacement ou de représentation payés à des membres du Conseil d'Administration.

Tout acte ou prestation effectué au bénéfice du tiers au nom de l'association par l'un de ses membres devra être autorisé par le conseil d'administration.

La rémunération de prestations pour le compte de l'association doit être autorisée par le conseil d'administration ou toute personne dûment mandatée par lui.

Si l'acte ou la prestation au nom de l'association est rétribué, il ne pourra donner lieu à rétribution personnelle, l'association étant dans ce cas la seule bénéficiaire autorisée, en la personne de son trésorier.

## ARTICLE 13 - Assemblée Générale Extraordinaire
Si besoin est, ou sur demande du quart des membres, le Président convoque une Assemblée Générale Extraordinaire. Les conditions de convocation sont identiques à celles de l'Assemblée Générale Ordinaire.

L'ordre du jour est la modification des statuts ou la dissolution. Les délibérations sont prises à la majorité des deux tiers des membres présents.

## ARTICLE 14 - Dissolution
En cas de dissolution prononcée par l'Assemblée Générale Extraordinaire, convoquée selon les modalités prévues à l'article 13, un ou plusieurs liquidateurs sont nommés par celle-ci et l'actif, s'il y a lieu, est dévolu à une association ayant des buts similaires, conformément à l'article 9 de la loi du 1er juillet 1901 et au décret du 16 août 1901.

## ARTICLE 15 - Règlement intérieur
Un règlement intérieur peut être établi par le Conseil d'administration qui le fait alors approuver par l'Assemblée Générale.
Ce règlement éventuel est destiné à fixer les divers points non prévus par les statuts, notamment ceux qui ont trait à l'administration interne de l'association.

## ARTICLE 16 - Affiliation
L'association n'est pas affiliée.

## ARTICLE 17 - Sectorisation
L'association n'est pas sectorisée.