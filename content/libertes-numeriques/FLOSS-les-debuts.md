---
weight : 20
title : Logiciel Libre - Les débuts
draft : false
ShowToc : true
TocOpen : false
---

L’histoire du Logiciel Libre commence dans les années 1970 (bien avant Google, Facebook et Apple) et ce qui allait devenir Internet n’est qu’à l’état embryonnaire. 

## Le M.I.T. 

Ingénieur au laboratoire d’intelligence artificielle (AI Lab) du M.I.T (Massachusetts Institute of Technology), Richard Stallman est un habitué du code et, comme tous les chercheurs de cette époque, le partage de celui-ci est naturel. 

À cette époque, le concept de logiciel Libre n’existe pas. On peut même dire qu’il n’existe que ça ! 

Quand vous achetez un jeu, un logiciel de traitement de texte ou une imprimante, seul le code source est fourni. Le matériel informatique n’a qu’une interface assez basique : la ligne de commande. 

## L’imprimante est encore en panne 

Le AI Lab reçoit en cadeau de Xerox une nouvelle imprimante laser, beaucoup plus précise que l’ancienne imprimante matricielle (imprimante à aiguilles). Le cadeau est le bienvenu, mais les chercheurs font régulièrement face à des bourrages papier. 

Doté d’un PDP-10, Stallman va chercher le code du driver de l’imprimante pour faire, comme à son habitude, une modification de celui-ci pour l’optimiser mais il n’en trouve aucune trace. 

Frustré de ne pas trouver de solutions mais loin d’être fâché contre le constructeur, il mène quelques recherches sans trop de résultat. Il ne prend même pas le temps de contacter Xerox. 

Quelques semaines plus tard, une nouvelle parvient aux oreilles de Stallman : Un chercheur de l’université de Carnegie Melon posséderait une copie du code source. 

## Un accord désaccordé

La distance qui sépare les deux chercheurs est de 930 km, ce qui n’est pas loin à l’échelle américaine. 

Stallman se rend donc sur place et va rencontrer le chercheur en question. Il ne va cependant pas trouver ce qu’il cherche. 

Un accord liant l’université et Xerox n’autorise l’étude du code source qu’en interne. Son collègue refuse donc de le partager. 

Stallman a vécut cette situation comme une trahison de ce qu’il appelle “l’éthique du hacker”. Le terme “hacker” signifiant une personne cherchant à contourner l’usage premier d’un matériel ou d’un logiciel pour l’adapter à ses propres besoins. 

## Le projet GNU et la FSF 

C’est cet évènement qui le mène à créer le projet GNU visant à créer un système d’exploitation entièrement libre. 

Pour cadrer ce projet, il créé la Free Software Foundation, à l’origine des licences GPL (General Public Licence) très populaires dans la communauté du logiciel libre. 

Ces licences seront notamment utilisées par de nombreux projets et serviront de modèles pour d’autres licences plus spécifiques. 

## Quelques pépins 

Travaillant au développement de l’éditeur de texte Emacs, Stallman trouve illusoire de développer des logiciels libres à destination d’un système d’exploitation “privateur” (ou “propriétaire” pour le commun des mortels). 

En 1990, le projet GNU est un ensemble de logiciels dédiés chacun à une tâche spécifique sans réel liens entre eux, cela ne pouvant pas constituer un système d’exploitation en tant que tel. Il lui manque un composant fondamental qu’on appelle « le noyau ». 

Ce noyau consiste à faire communiquer les ressources matérielles avec les logiciels. Pour mener à bien cet objectif, Stallman lance la même année le projet “GNU/Hurd”. 

Le projet ne rencontre pas un succès fabuleux. Le développement est lent et peu de personnes sont prêtes à apporter leurs contributions. 

Pour vous donner une idée de l’avancée du projet, à l’heure où vous lirez ces lignes aucune version stable n’existera.  

## Un noyau 

En 1991, Linus Torvalds est étudiant à l’Université d’Helsinki et utilise un Commodore Vic 20 sur lequel il a écrit quelques logiciels ; principalement des jeux et des éditeurs de texte. 

Vu la faible disponibilité des terminaux physiques nécessaires pour accéder au serveur de l’université, Torvalds va développer son émulateur de terminal qu’il va pouvoir lancer sur sa machine connectée à un modem. 

Cet émulateur va assez vite évoluer et plusieurs fonctionnalités vont y êtes ajoutés. Le projet prend de l’ampleur et Trovalds entreprend de créer un noyau qu’il va appeler « Freaks ». 

C’est Ari Lemmke, administrateur d’un serveur de fichiers, qui a trouvé le nom de Linux. Il a créé un répertoire de ce nom pour y stocker le travail de Torvalds. 

## La première version publique 

C’est le 25 août 1991 que Linus Torvalds publie sur le groupe de discussion “comp.os.minix” le message suivant : 

> Bonjour tout ceux utilisant minix - Je fais un système d'exploitation (gratuit) (juste un passe-temps, ne sera pas grand et professionnel comme gnu) pour 386(486) AT clones.  
> 
> Cela se prépare depuis avril et commence à être opérationnel. J'aimerais avoir des commentaires sur les choses que les gens aiment ou n’aiment pas dans minix, car mon OS lui ressemble quelque peu (même disposition physique du système de fichiers (pour des raisons pratiques), entre autres).  
> 
> J'ai actuellement porté bash(1.08) et gcc(1.40), et les choses semblent fonctionner. Cela implique que je vais obtenir quelque chose de pratique dans quelques mois, et j'aimerais savoir quelles fonctionnalités la plupart des gens voudraient. 
>
> Toutes les suggestions sont les bienvenues, mais je ne promets pas de les mettre en œuvre :-)  
> 
> Linus (torvalds [at] kruuna.helsinki.fi) 
>
> PS. Oui – il est libre de tout code minix, et il a un FS multi-threadé. Il n'est PAS portable (utilise la commutation de tâches 386, etc.), et il ne supportera probablement jamais autre chose que les disques durs AT, car c'est tout ce que j'ai :-(.
> 
>  —Linus Torvalds 

Dans ce message technique, nous pouvons constater qu’il ne se doute pas de l’ampleur que va prendre son « passe-temps ». 

## Retour vers le présent

Stallman et Torvalds ont été déclencheurs, avec leurs travaux, d’un mouvement mondial constitué de multiples communautés de développeurs. 30 ans plus tard, les logiciels libres sont omniprésents dans tous les domaines (matériel embarqué, ordinateurs, consoles de jeux, ...). 
