---
menu : main
weight : 20
title : Les Libertés numériques
draft : false
---

Les logiciels libres et les libertés numériques ont été pensés pour promouvoir la libre utilisations des programmes informatiques et les droits des utilisateurs. 

Les articles ci-dessous retracent l'histoire et les enjeux entourant ces deux notions.

> Cette partie du site est en cours de rédaction. N’hésitez pas à revenir ! 