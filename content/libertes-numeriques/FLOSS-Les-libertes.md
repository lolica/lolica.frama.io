---
weight : 10
title : Logiciel Libre - Les 4 libertés fondamentales
draft : true
---

Un logiciel libre se différencie des autres logiciels par son développement et les droits qu'il offre aux utilisateurs.

Ces libertés ont étés définies à la suite des travaux de Richard Stallman dans le cadre de la Free Software Fondation. Pour plus d'informations sur l'histoire du mouvement du Logiciel Libre, un article plus complet est [disponible ici](../floss-les-debuts).

## Les libertés 

Les libertés ci-dessous sont numérotées de 0 à 3 et constituent en elles-mêmes la définition de ce qu'est un logiciel libre.

La raison de cette numérotation est historique. En effet, dans les années 1990, il n'y a que 3 libertés et il est devenu nécessaire de mentionner la liberté d'exécution du programme.

Etant-donnée que cette liberté est la plus basique des 3, mais aussi pour éviter une renumérotation, il a été décidé de l'appeller "Liberté 0". 

### 0 - Liberté d'exécuter le programme

Elle permet aux utilisateurs d'exécuter le logiciel à leur guise, sans aucune restriction quant à son utilisation ou son but. Cela signifie que vous pouvez utiliser le logiciel pour vos besoins personnels, professionnels, éducatifs, ou tout autre objectif, sans avoir à demander l'autorisation à quiconque.

### 1 - Liberté d'étudier le fonctionnement du programme

Elle garantit aux utilisateurs la possibilité d'étudier, de comprendre et de modifier le code source d'un programme, ce qui favorise l'innovation, l'apprentissage et l'adaptation aux besoins individuels.

Cela implique que les utilisateurs aient accès au code source du logiciel, c'est-à-dire le texte écrit dans un langage de programmation compréhensible par les humains. 

### 2 - Liberté de redistribuer des copies du logiciel

Cette liberté garantie que les utilisateurs puissent copier et partager le logiciel librement, sans avoir à demander l'autorisation à l'auteur original.

Cela ne s'applique pas seulement au logiciel original, mais aussi aux versions modifiées ou améliorées du programme. Ainsi, les utilisateurs qui apportent des modifications au code source peuvent également redistribuer leurs versions modifiées, à condition de respecter les conditions de la licence.

### 3 - Liberté d'améliorer le programme

Cette liberté garantit que les utilisateurs puissent apporter des modifications au code source du logiciel et distribuer leurs versions améliorées ou modifiées. Ainsi, les utilisateurs peuvent non seulement adapter le logiciel à leurs propres besoins, mais aussi partager leurs améliorations avec les autres.

De la même manière que la liberté 1, l'accès au code source est nécessaire.

## Logiciel Libre ne veut pas dire « Non commercial »

Le terme « Logiciel Libre » est souvent associé à tord à la gratuité. Comme nous venons de le voir avec ces 4 libertés fondamentales, la notion de "libre" se réfère aux libertés accordées aux utilisateurs.

Bien que de nombreux Logiciels Libres sont effectivement gratuits, certains peuvent avoir un coût. Ces coûts sont souvents associés à des services de supports et/ou d'installation.